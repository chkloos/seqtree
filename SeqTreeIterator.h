// kate: indent-mode cstyle; indent-width 4; replace-tabs off; tab-width 4; 

/*
 * Copyright (c) Christoph Kloos 2018
 * This file is subject to the terms and conditions of the
 * MIT License | https://opensource.org/licenses/MIT
 * SPDX-License-Identifier: MIT
 */

/*!
 * \file
 * \author Christoph Kloos
 * \copyright (c) Christoph Kloos 2018
 */

#pragma once

#include <memory>
#include <iterator>
#include <type_traits> // std::conditional
#include <cstddef>
#include "SeqTreeClass.h"

namespace SeqTreeIterator_PRIVATE
{
	
	template <class C>
	class SeqTreeIteratorBase
	{
	public:
		typedef C Container;
		
// 		SeqTreeIteratorBase() : anchor(NULL), current(NULL), outside(0) {};
		SeqTreeIteratorBase(Container& anchor, Container& start, int outside)
			: a_anchor(&anchor), a_current(&start), a_outside(outside) {}
		
		inline Container* current() const { return a_current; }
		inline bool isBegin() const { return a_outside == -1; }
		inline bool isEnd() const { return a_outside == 1; }
		inline bool isInside() const { return !a_outside; }
		
		inline void jumpBegin(bool place_outside)
		{
			assert(a_anchor);
			a_current = a_anchor;
			a_outside = place_outside ? -1 : 0;
		}
		
		inline bool operator==(const SeqTreeIteratorBase& rhs) const
		{
			return this->a_anchor == rhs.a_anchor &&
			( (this->a_outside == rhs.a_outside && this->a_outside) || 
			( this->a_current == rhs.a_current && this->a_outside == rhs.a_outside ) );
		}
		
		inline bool operator!=(const SeqTreeIteratorBase& rhs) const { return ! operator==(rhs); }

	protected:
		
		typedef typename Container::SeqTreeClassT::SharedT SharedT;
		typedef typename Container::SeqTreeClassT::WeakT WeakT;
		typedef typename Container::SeqTreeClassT::ChildT ChildT;
		typedef typename Container::SeqTreeClassT::ParentT ParentT;

		template <bool b = std::is_same<ChildT, SharedT>::value, typename std::enable_if<b>::type* = nullptr>
		static inline Container* cToPtr(const std::shared_ptr<Container>& that)  { return that.get(); }
		template <bool b = std::is_same<ChildT, WeakT>::value, typename std::enable_if<b>::type* = nullptr>
		static inline Container* cToPtr(const std::weak_ptr<Container>& that)  { return that.lock().get(); }
		
		static inline Container* cToPtr(const Container* that)  { return that; }
		
		template <bool b = std::is_same<ParentT, SharedT>::value, typename std::enable_if<b>::type* = nullptr>
		static inline Container* pToPtr(const std::shared_ptr<Container>& that)  { return that.get(); }
		template <bool b = std::is_same<ParentT, WeakT>::value, typename std::enable_if<b>::type* = nullptr>
		static inline Container* pToPtr(const std::weak_ptr<Container>& that)  { return that.lock().get(); }
		
		static inline Container* pToPtr(const Container* that)  { return that; }
		
		Container* a_anchor;
		Container* a_current;
		int a_outside; // -1 for begin, 0 for inside, 1 for end
	};
	
	template <class C>
	class SeqTreeDXIterator : public SeqTreeIteratorBase<C>
	{
	public:
        typedef C Container;
        
		SeqTreeDXIterator(Container& anchor, Container& startpos, int outside) : SeqTreeIteratorBase<C>(anchor, startpos, outside) {}
		
		void jumpEnd(bool place_outside)
		{
			this->a_current = this->a_anchor;
			this->a_outside = place_outside ? 1 : 0;
			while (this->a_current->children().size()) this->a_current = this->cToPtr(*(this->a_current->children().cend()-1));
		}
		
    protected:
		bool forward_previous()
		{
            // general part
			if (this->a_outside == -1 || !this->a_anchor || !this->a_current) return false;
			if (this->a_outside == 1) { this->a_outside = 0; return true; }
			if (this->a_current == this->a_anchor || this->a_current->isRoot()) { this->a_outside = -1; return true; }
            
            // individual part
            const typename Container::IndexT current_idx = this->a_current->index();
            if (current_idx) // go to previous sibling and dive to the right outer leaf
            {
                this->a_current = this->cToPtr(this->a_current->parent()->child(current_idx-1)); // previous sibling
                while (this->a_current->children().size()) this->a_current = this->cToPtr(*(this->a_current->children().cend()-1));
                return true;
            }
            else // go to parent
            {
                this->a_current = this->pToPtr(this->a_current->parent());
                return true;
            }
		}
		
		bool forward_next()
		{
            // general part
			if (this->a_outside == 1 || !this->a_anchor) return false;
			if (this->a_outside == -1 || !this->a_current) { this->a_current = this->a_anchor; this->a_outside = 0; return true; }

            // individual part
            if (this->a_current->children().size()) { this->a_current = this->cToPtr(this->a_current->child(0)); return true; } // dive in
            if (this->a_current == this->a_anchor) { this->a_outside = 1; return true; } // after trying to go deeper!
            
            // go to next sibling or go higher and then to next sibling
            Container* new_current = this->a_current;
            while (true)
            {	
                if (new_current->isRoot()) { this->a_outside = 1; return true; } // there is no parent, do not change current
                const typename Container::IndexT next_idx = new_current->index()+1;
                if (new_current->parent()->children().size() > next_idx) // next sibling: go to parent and then to next child
                    { this->a_current = this->cToPtr(new_current->parent()->child(next_idx)); return true; }
                if (this->pToPtr(new_current->parent()) == this->a_anchor) // don't go higher than anchor, and keep current as last
                    { this->a_outside = 1; return true; }
                new_current = this->pToPtr(new_current->parent()); // go higher
            }
            return false; // never reached
		}
	};

	template <class C>
	class SeqTreeDFIterator : public SeqTreeDXIterator<C>
	{
	public:
        typedef C Container;
		SeqTreeDFIterator(Container& anchor, bool is_end) : SeqTreeDXIterator<C>(anchor, anchor, 0)
		{
			if (is_end) this->jumpEnd(true);
			else this->jumpBegin(false);
		}
		
        inline bool previous() { return this->forward_previous(); }
        inline bool next() { return this->forward_next(); }
	};
	
	template <class C>
	class SeqTreeDRIterator : public SeqTreeDXIterator<C>
	{
	public:
        typedef C Container;
		SeqTreeDRIterator(Container& anchor, bool is_end) : SeqTreeDXIterator<C>(anchor, anchor, 0)
		{
			if (is_end) this->jumpBegin(true);
			else this->jumpEnd(false);
		}
		
        inline bool previous() { return this->forward_next(); }
        inline bool next() { return this->forward_previous(); }
	};
}

// #################################################################################

template <class C, bool is_const, bool is_reverse, bool return_node>
class SeqTreeDIterator // public std::iterator<std::bidirectional_iterator_tag, C::value_type >
{
public:
	// additional typedefs (which are not part of std::iterator)
	typedef C Container;
// 	typedef SeqTreeDIterator selftype;
    typedef typename std::conditional<is_const, const Container, Container>::type CContainer;
	
private:
    typedef typename std::conditional<is_reverse,
        SeqTreeIterator_PRIVATE::SeqTreeDRIterator<CContainer>,
        SeqTreeIterator_PRIVATE::SeqTreeDFIterator<CContainer>
        >::type ItrbaseT;
    ItrbaseT a_itrbase;
    
public:
	// typedefs from from std::iterator (they would not really been inherited):
	typedef std::bidirectional_iterator_tag iterator_category; // or random_access_iterator_tag ...
	typedef typename std::conditional<return_node, Container, typename Container::DerivedT::value_type>::type value_type;
	typedef typename std::conditional<is_const, const value_type*, value_type*>::type pointer;
	typedef typename std::conditional<is_const, const value_type&, value_type&>::type reference;
	
	explicit SeqTreeDIterator(CContainer& anchor, bool is_end) : a_itrbase(anchor, is_end) {}
	
	inline bool operator==(const SeqTreeDIterator& rhs) const { return this->a_itrbase == rhs.a_itrbase; }
	inline bool operator!=(const SeqTreeDIterator& rhs) const { return this->a_itrbase != rhs.a_itrbase; }
	inline SeqTreeDIterator& operator++() { a_itrbase.next(); return *this; }
	inline SeqTreeDIterator& operator--() { a_itrbase.previous(); return *this; }
	inline SeqTreeDIterator operator++(int dummy) { const SeqTreeDIterator tmp(*this); ++(*this); return tmp; }
	inline SeqTreeDIterator operator--(int dummy) { const SeqTreeDIterator tmp(*this); --(*this); return tmp; }
	
	template <bool b = return_node, typename std::enable_if<b>::type* = nullptr>
	inline reference operator*() const { return *(a_itrbase.current()); }
    template <bool b = return_node, typename std::enable_if<b>::type* = nullptr>
	inline pointer operator->() const { return &(*(a_itrbase.current())); }
	
	template <bool b = return_node, bool c = is_const, typename std::enable_if<!b && !c>::type* = nullptr>
	inline reference operator*() const { return (a_itrbase.current())->value(); }
    template <bool b = return_node, bool c = is_const, typename std::enable_if<!b && !c>::type* = nullptr>
	inline pointer operator->() const { return &((a_itrbase.current())->value()); }
	template <bool b = return_node, bool c = is_const, typename std::enable_if<!b && c>::type* = nullptr>
	inline reference operator*() const { return (a_itrbase.current())->cvalue(); }
    template <bool b = return_node, bool c = is_const, typename std::enable_if<!b && c>::type* = nullptr>
	inline pointer operator->() const { return &((a_itrbase.current())->cvalue()); }
	
	// TODO non-const to const copy constructor, also operator= ...
};


template <class C, bool is_const, bool is_reverse, bool return_node>
class SeqTreeCIterator // public std::iterator<std::random_access_iterator_tag, C::value_type >
{
public:
	// additional typedefs (which are not part of std::iterator)
	typedef C Container;
// 	typedef SeqTreeCIterator selftype;
    typedef typename std::conditional<is_const, const Container, Container>::type CContainer;
	
private:
    typedef typename std::conditional<is_reverse,
		typename CContainer::SeqTreeClassT::ChildrenT::const_reverse_iterator,
		typename CContainer::SeqTreeClassT::ChildrenT::const_iterator
        >::type ItrbaseT;
	
    ItrbaseT a_itrbase;
    
public:
	// typedefs from from std::iterator (they would not really been inherited):
	typedef std::random_access_iterator_tag iterator_category; // or bidirectional_iterator_tag ...
	typedef typename std::conditional<return_node, Container, typename Container::DerivedT::value_type>::type value_type;
	typedef typename std::conditional<is_const, const value_type*, value_type*>::type pointer;
	typedef typename std::conditional<is_const, const value_type&, value_type&>::type reference;
	
	template <bool b = is_reverse, typename std::enable_if<b>::type* = nullptr>
	explicit SeqTreeCIterator(CContainer& anchor, bool is_end)
		{ a_itrbase = is_end ? anchor.children().crend() : anchor.children().crbegin(); }
	
	template <bool b = is_reverse, typename std::enable_if<!b>::type* = nullptr>
	explicit SeqTreeCIterator(CContainer& anchor, bool is_end)
		{ a_itrbase = is_end ? anchor.children().cend() : anchor.children().cbegin(); }
	
	inline bool operator==(const SeqTreeCIterator& rhs) const { return this->a_itrbase == rhs.a_itrbase; }
	inline bool operator!=(const SeqTreeCIterator& rhs) const { return this->a_itrbase != rhs.a_itrbase; }
	inline SeqTreeCIterator& operator++() { a_itrbase++; return *this; }
	inline SeqTreeCIterator& operator--() { a_itrbase--; return *this; }
	inline SeqTreeCIterator operator++(int dummy) { const SeqTreeCIterator tmp(*this); ++(*this); return tmp; }
	inline SeqTreeCIterator operator--(int dummy) { const SeqTreeCIterator tmp(*this); --(*this); return tmp; }
	
	template <bool b = return_node, typename std::enable_if<b>::type* = nullptr>
	inline reference operator*() const { return *(*a_itrbase); }
    template <bool b = return_node, typename std::enable_if<b>::type* = nullptr>
	inline pointer operator->() const { return &(*(*a_itrbase)); }
	
	template <bool b = return_node, bool c = is_const, typename std::enable_if<!b && !c>::type* = nullptr>
	inline reference operator*() const { return (*a_itrbase)->value(); }
    template <bool b = return_node, bool c = is_const, typename std::enable_if<!b && !c>::type* = nullptr>
	inline pointer operator->() const { return &((*a_itrbase)->value()); }
	template <bool b = return_node, bool c = is_const, typename std::enable_if<!b && c>::type* = nullptr>
	inline reference operator*() const { return (*a_itrbase)->cvalue(); }
    template <bool b = return_node, bool c = is_const, typename std::enable_if<!b && c>::type* = nullptr>
	inline pointer operator->() const { return &((*a_itrbase)->cvalue()); }
	
	inline bool operator<(const SeqTreeCIterator& rhs) const { return this->a_itrbase < rhs.a_itrbase; }
	inline bool operator>(const SeqTreeCIterator& rhs) const { return this->a_itrbase > rhs.a_itrbase; }
	inline bool operator<=(const SeqTreeCIterator& rhs) const { return this->a_itrbase <= rhs.a_itrbase; }
	inline bool operator>=(const SeqTreeCIterator& rhs) const { return this->a_itrbase >= rhs.a_itrbase; }
	inline SeqTreeCIterator& operator+=(const SeqTreeCIterator& rhs) { this->a_itrbase += rhs.a_itrbase; return *this; }
	inline SeqTreeCIterator& operator-=(const SeqTreeCIterator& rhs) { this->a_itrbase -= rhs.a_itrbase; return *this; }
	
	template <bool b = return_node, typename std::enable_if<b>::type* = nullptr>
	inline reference operator[](typename Container::IndexT idx) const { return *(this->a_itrbase[idx]); }
	template <bool b = return_node, bool c = is_const, typename std::enable_if<!b && !c>::type* = nullptr>
	inline reference operator[](typename Container::IndexT idx) const { return (this->a_itrbase[idx])->value; }
	template <bool b = return_node, bool c = is_const, typename std::enable_if<!b && c>::type* = nullptr>
	inline reference operator[](typename Container::IndexT idx) const { return (this->a_itrbase[idx])->cvalue; }
};

