// kate: indent-mode cstyle; indent-width 4; replace-tabs off; tab-width 4; 

/*
 * Copyright (c) Christoph Kloos 2018
 * This file is subject to the terms and conditions of the
 * MIT License | https://opensource.org/licenses/MIT
 * SPDX-License-Identifier: MIT
 */

/*!
 * \file
 * \author Christoph Kloos
 * \copyright (c) Christoph Kloos 2018
 */

#pragma once

#include <algorithm>
#include "SeqTreeClass.h"
#include "SeqTreeIterator.h"

template <class T, class IdxT = size_t, char anchor_mode = 'P'>
class SeqTree : public SeqTreeClass<SeqTree<T, size_t, anchor_mode>, IdxT, anchor_mode>
{
public:
	typedef T value_type; // std compliant name
	typedef const T const_value_type;
	typedef SeqTreeClass<SeqTree<T, size_t, anchor_mode>, IdxT, anchor_mode> SeqTreeClassT;
    typedef IdxT IndexT;
	
	typedef SeqTreeDIterator<SeqTree, false, false, false> Depth_VIterator;
	typedef SeqTreeDIterator<SeqTree, false, true, false> Depth_RVIterator;
	typedef SeqTreeDIterator<SeqTree, true, false, false> Depth_CVIterator;
	typedef SeqTreeDIterator<SeqTree, true, true, false> Depth_CRVIterator;
	
	inline Depth_VIterator depth_VBegin() { return Depth_VIterator(*this, false); }
	inline Depth_VIterator depth_VEnd() { return Depth_VIterator(*this, true); }
	inline Depth_RVIterator depth_RVBegin() { return Depth_RVIterator(*this, false); }
	inline Depth_RVIterator depth_RVEnd() { return Depth_RVIterator(*this, true); }
	inline Depth_CVIterator depth_CVBegin() const { return Depth_CVIterator(*this, false); }
	inline Depth_CVIterator depth_CVEnd() const { return Depth_CVIterator(*this, true); }
	inline Depth_CRVIterator depth_CRVBegin() const { return Depth_CRVIterator(*this, false); }
	inline Depth_CRVIterator depth_CRVEnd() const { return Depth_CRVIterator(*this, true); }
	
	typedef SeqTreeCIterator<SeqTree, false, false, false> Child_VIterator;
	typedef SeqTreeCIterator<SeqTree, false, true, false> Child_RVIterator;
	typedef SeqTreeCIterator<SeqTree, true, false, false> Child_CVIterator;
	typedef SeqTreeCIterator<SeqTree, true, true, false> Child_CRVIterator;
	
	inline Child_VIterator child_VBegin() { return Child_VIterator(*this, false); }
	inline Child_VIterator child_VEnd() { return Child_VIterator(*this, true); }
	inline Child_RVIterator child_RVBegin() { return Child_RVIterator(*this, false); }
	inline Child_RVIterator child_RVEnd() { return Child_RVIterator(*this, true); }
	inline Child_CVIterator child_CVBegin() const { return Child_CVIterator(*this, false); }
	inline Child_CVIterator child_CVEnd() const { return Child_CVIterator(*this, true); }
	inline Child_CRVIterator child_CRVBegin() const { return Child_CRVIterator(*this, false); }
	inline Child_CRVIterator child_CRVEnd() const { return Child_CRVIterator(*this, true); }
	
	inline SeqTree() : SeqTreeClassT() {}
	inline SeqTree(const value_type& val) : SeqTreeClassT(), val(val) {}
	
	template <char x = anchor_mode, typename = std::enable_if<x == 'P'>> // only for non-shared
	SeqTree(const value_type& val, typename SeqTreeClassT::SharedT parent) : SeqTreeClassT(parent), val(val) {}
	
	template <char x = anchor_mode, typename = std::enable_if<x == 'P'>>
	SeqTree(const value_type& val, typename SeqTreeClassT::SharedT parent, IdxT index) : SeqTreeClassT(parent, index), val(val) {}
	
	
	inline SeqTree(value_type&& val) : SeqTreeClassT(), val(std::move(val)) {}
	
	template <char x = anchor_mode, typename = std::enable_if<x == 'P'>>
	SeqTree(value_type&& val, typename SeqTreeClassT::SharedT parent) : SeqTreeClassT(parent), val(std::move(val)) {}
	
	template <char x = anchor_mode, typename = std::enable_if<x == 'P'>>
	SeqTree(value_type&& val, typename SeqTreeClassT::SharedT parent, IdxT index) : SeqTreeClassT(parent, index), val(std::move(val)) {}
	
	virtual ~SeqTree() {}
	
	inline void swapContent(SeqTree& other) noexcept { std::swap(val, other.val); SeqTreeClassT::swapContent(other); }
	
	inline SeqTree(const SeqTree & other) : SeqTreeClassT(), val(other.val)
	{
		// note that SeqTreeClassT(other) is deleted in the base class. This copy constructor must not do a deep copy,
		// because at many places a copy based on the values is necessary (in this sub class only one value).
		// If a deep copy is needed, then deepCopy should be used.
	}
	
	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	inline SeqTree(SeqTree && other) noexcept : SeqTreeClassT(std::move(static_cast<SeqTreeClassT&>(other))), val(std::move(other.val)) {}
	
	// shared_from_this() does not work during construction, and only moving the value does not make sense!
	template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	inline SeqTree(SeqTree && other) noexcept = delete;
	
	inline SeqTree& operator=(const SeqTree & rhs)
	{
		// note: if a deep copy is needed, use copyChildrenTo<true>(rhs) after the assignment
		// therefore SeqTreeClass does not have this operator - there are no meaningful values besides the children
		
		if (this != &rhs)
		{
			// SeqTreeClassT::operator=(rhs); // deleted in the base class
			// nothing to remove/delete in this sub-class
			this->val = rhs.val;
		}
		return *this;
	}
	
	inline SeqTree& operator=(SeqTree && rhs) noexcept
	{
		if (this != &rhs)
		{
			SeqTreeClassT::operator=(std::move(rhs));
			// nothing to remove/delete in this sub-class
			this->val = std::move(rhs.val);
		}
		return *this;
	}
	
	inline value_type& value() { return val; };
	inline const value_type& cvalue() const { return val; };
	
	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	inline SeqTree* push(const value_type& val) { return new SeqTree(val, this); }
	template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	inline typename SeqTreeClassT::SharedT push(const value_type& val)
        { auto result = std::make_shared<SeqTree>(val); result->moveTo(this->shared_from_this()); return result; }
	
	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	inline SeqTree* push(value_type&& val) { return new SeqTree(std::move(val), this); }
	template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	inline typename SeqTreeClassT::SharedT push(value_type&& val)
        { auto result = std::make_shared<SeqTree>(std::move(val)); result->moveTo(this->shared_from_this()); return result; }
	
	
	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	inline SeqTree* insert(const value_type& val, IdxT index)
		{ return new SeqTree(val, this, index); }
	template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	inline typename SeqTreeClassT::SharedT insert(const value_type& val, IdxT index)
		{ auto result = std::make_shared<SeqTree>(val); result->moveTo(this->shared_from_this(), index); return result; }
    
	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	inline SeqTree* insert(value_type&& val, IdxT index)
		{ return new SeqTree(std::move(val), this, index); }
	template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	inline typename SeqTreeClassT::SharedT insert(value_type&& val, IdxT index)
		{ auto result = std::make_shared<SeqTree>(std::move(val)); result->moveTo(this->shared_from_this(), index); return result; }
	
	bool operator==(const SeqTree& rhs) const;
	bool operator<(const SeqTree& rhs) const;
	void sortChildren(bool descending = false, bool deep = false);
	
protected:
	value_type val;
	
private:
	static inline bool cmp_sort_asc(const SeqTree* lhs, const SeqTree* rhs) { return lhs->val < rhs->val; };
	static inline bool cmp_sort_desc(const SeqTree* lhs, const SeqTree* rhs) { return rhs->val < lhs->val; };
};


template <class value_type, class IdxT, char anchor_mode>
bool SeqTree<value_type, IdxT, anchor_mode>::operator==(const SeqTree& rhs) const
{
	return this->val == rhs.val;
}

template <class value_type, class IdxT, char anchor_mode>
bool SeqTree<value_type, IdxT, anchor_mode>::operator<(const SeqTree& rhs) const
{
	return this->val < rhs.val;
}

template <class T, class IdxT, char anchor_mode>
void SeqTree<T, IdxT, anchor_mode>::sortChildren(bool descending, bool deep)
{
	if (descending) std::sort(SeqTreeClassT::a_children.begin(), SeqTreeClassT::a_children.end(), SeqTree::cmp_sort_desc);
	else std::sort(SeqTreeClassT::a_children.begin(), SeqTreeClassT::a_children.end(), SeqTree::cmp_sort_asc);
	
	if (deep)
	{
		for (auto i = SeqTreeClassT::a_children.begin(); i < SeqTreeClassT::a_children.end(); ++i)
			(*i)->SortChildren(descending, true);
	}
}
