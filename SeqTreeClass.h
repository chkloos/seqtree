// kate: indent-mode cstyle; indent-width 4; replace-tabs off; tab-width 4; 

/*
 * Copyright (c) Christoph Kloos 2018
 * This file is subject to the terms and conditions of the
 * MIT License | https://opensource.org/licenses/MIT
 * SPDX-License-Identifier: MIT
 */

/*!
 * \file
 * \author Christoph Kloos
 * \copyright (c) Christoph Kloos 2018
 */

#pragma once

#include <cassert>
#include <vector>
#include <algorithm>
#include <utility>
#include <memory>
#include <functional>
#include <type_traits>

// Anchor mode:
// 'P' pointer: Derived* for childran and parent
// 'R' root holds the children: children:shared_ptr<Derived>   parent:weak_ptr<Derived>
// 'L' leaf holds its parent:   children:weak_ptr<Derived>     parent:shared_ptr<Derived>

// needed to allow conditional inheritance from nothing
namespace private_namespace_dummy { class DUMMY{}; }

template <class C, bool is_const, bool is_reverse, bool return_node> class SeqTreeDIterator;
template <class C, bool is_const, bool is_reverse, bool return_node> class SeqTreeCIterator;

template <class Derived, class IdxT = size_t, char anchor_mode = 'P'>
class SeqTreeClass : public std::conditional<anchor_mode != 'P', std::enable_shared_from_this<Derived>, private_namespace_dummy::DUMMY>::type
{
public:
	typedef Derived DerivedT; // needed to access Derived from other classes, like iterator
	typedef typename std::conditional<anchor_mode =='P', Derived*, std::shared_ptr<Derived> >::type SharedT;
	typedef typename std::conditional<anchor_mode =='P', Derived*, std::weak_ptr<Derived> >::type WeakT;
	typedef typename std::conditional<anchor_mode=='R', SharedT, WeakT>::type ChildT;
	typedef typename std::conditional<anchor_mode=='R', WeakT, SharedT>::type ParentT;
 
	
	typedef std::vector<ChildT> ChildrenT;
	typedef IdxT IndexT;
	
	// ######### <iterator stuff> ##############
	
// 	template <class C, bool is_const, bool is_reverse, bool return_node, char itype>
// 	friend class SeqTreeDIterator;
	
	typedef SeqTreeDIterator<Derived, false, false, true> Depth_Iterator;
	typedef SeqTreeDIterator<Derived, false, true, true> Depth_RIterator;
	typedef SeqTreeDIterator<Derived, true, false, true> Depth_CIterator;
	typedef SeqTreeDIterator<Derived, true, true, true> Depth_CRIterator;
	
	inline Depth_Iterator depth_Begin() { return Depth_Iterator(*static_cast<Derived*>(this), false); }
	inline Depth_Iterator depth_End() { return Depth_Iterator(*static_cast<Derived*>(this), true); }
	inline Depth_RIterator depth_RBegin() { return Depth_RIterator(*static_cast<Derived*>(this), false); }
	inline Depth_RIterator depth_REnd() { return Depth_RIterator(*static_cast<Derived*>(this), true); }
	inline Depth_CIterator depth_CBegin() const { return Depth_CIterator(*static_cast<const Derived*>(this), false); }
	inline Depth_CIterator depth_CEnd() const { return Depth_CIterator(*static_cast<const Derived*>(this), true); }
	inline Depth_CRIterator depth_CRBegin() const { return Depth_CRIterator(*static_cast<const Derived*>(this), false); }
	inline Depth_CRIterator depth_CREnd() const { return Depth_CRIterator(*static_cast<const Derived*>(this), true); }

	// *** child iterator ***
	
// 	template <class C, bool is_const, bool is_reverse, bool return_node>
// 	friend class SeqTreeCIterator;
	
	typedef SeqTreeCIterator<Derived, false, false, true> Child_Iterator;
	typedef SeqTreeCIterator<Derived, false, true, true> Child_RIterator;
	typedef SeqTreeCIterator<Derived, true, false, true> Child_CIterator;
	typedef SeqTreeCIterator<Derived, true, true, true> Child_CRIterator;
	
	inline Child_Iterator child_Begin() { return Child_Iterator(*static_cast<Derived*>(this), false); }
	inline Child_Iterator child_End() { return Child_Iterator(*static_cast<Derived*>(this), true); }
	inline Child_RIterator child_RBegin() { return Child_RIterator(*static_cast<Derived*>(this), false); }
	inline Child_RIterator child_REnd() { return Child_RIterator(*static_cast<Derived*>(this), true); }
	inline Child_CIterator child_CBegin() const { return Child_CIterator(*static_cast<const Derived*>(this), false); }
	inline Child_CIterator child_CEnd() const { return Child_CIterator(*static_cast<const Derived*>(this), true); }
	inline Child_CRIterator child_CRBegin() const { return Child_CRIterator(*static_cast<const Derived*>(this), false); }
	inline Child_CRIterator child_CREnd() const { return Child_CRIterator(*static_cast<const Derived*>(this), true); }

	// ######### </iterator stuff> ##############
	
	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	SeqTreeClass()
		: a_parent(nullptr), a_children(), a_index(0)
	{
	}

	template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	SeqTreeClass()
		: a_parent(), a_children(), a_index(0)
	{
	}

	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	SeqTreeClass(SharedT parent)
		: a_parent(parent), a_children()
	{
		if (!parent) return;
		a_index = static_cast<IdxT>(parent->a_children.size());
		parent->a_children.push_back(static_cast<Derived*>(this));
	}

	// not implemented. because shared_from_this does not work until the construction has finished
	// template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	// SeqTreeClass(SharedT parent)


	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	SeqTreeClass(SharedT parent, IdxT index)
		: a_parent(parent), a_children()
	{
		if (!parent) return;
		const IdxT sz = static_cast<IdxT>(parent->a_children.size());
		if (index >= sz)
		{
			a_index = sz;
			parent->a_children.push_back(static_cast<Derived*>(this));
		}
		else
		{
			a_index = index;
			parent->a_children.insert(parent->a_children.begin() + static_cast<size_t>(index), static_cast<Derived*>(this));
		}
	}

	// not implemented. because shared_from_this does not work until the construction has finished
	// template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	// SeqTreeClass(SharedT parent, IdxT index)


	virtual ~SeqTreeClass() { destruct(); } // workaround, because destructors must not be templates
	
private:
	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	inline void destruct()
	{
		for (auto i = a_children.begin(); i != a_children.end(); ++i)
		{
			delete *i;
		}
		if (a_parent) a_parent->a_children.erase(a_parent->a_children.begin() + index());
	}

	template <char x = anchor_mode, typename std::enable_if<x == 'R', char>::type* = nullptr>
	inline void destruct()
	{
		for (auto i = a_children.begin(); i != a_children.end(); ++i) (*i)->a_parent = ParentT();
		SharedT const parent = this->parent();
		if (parent) parent->a_children.erase(parent->a_children.begin() + destruction_index());
	}

	template <char x = anchor_mode, typename std::enable_if<x == 'L', char>::type* = nullptr>
	inline void destruct()
	{
		for (auto i = a_children.begin(); i != a_children.end(); ++i) i->lock()->a_parent = ParentT();
		SharedT const parent = this->parent();
		if (parent) parent->a_children.erase(parent->a_children.begin() + destruction_index());
	}

	template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	IdxT destruction_index() const
	{
		SharedT const parent = this->parent();
		if (!parent) return 0;
		
		// check, if the index number is valid anymore
		if (parent->a_children.size() > a_index)
        {
            const Derived* other = cToPtr(parent->a_children[a_index]);
            // note that other can be already NULL. because the shared pointer is already invalidated.
            if (!other || static_cast<const Derived*>(this) == other) return a_index;
        }
		
		// if not, determine the new number and fix the other index numbers too. Therefore use a reverse iterator,
		// because in normal case the index numbers from an arbitrary point to the end are invalid, and not
		// the index numbers from the beginning to a arbitrary point
		for (auto i = parent->a_children.rbegin(); i != parent->a_children.rend(); ++i)
		{
			// const auto current_index = i - parent->children.begin(); // if it would be a forward iterator
			const auto current_index = parent->a_children.rend() - i - 1;
			Derived* const ptr = cToPtr(*i);
			if (ptr == nullptr) return current_index; // because the destructor is already running, the shared pointer is typically already invalidated
			ptr->a_index = static_cast<IdxT>(current_index);
			if (static_cast<const Derived*>(this) == ptr) return current_index;
		}
		
		assert(false); // if this happens, the list of children at the parent was incomplete (which is a bug somewhere, but not here)
		return 0;
	}
	
	
public:

	template <char x = anchor_mode> // compile only if used
	IdxT index() const
	{
		SharedT const parent = this->parent();
		if (!parent) return 0;
		
		// check, if the index number is valid anymore
		if (parent->a_children.size() > a_index && static_cast<const Derived*>(this) == cToPtr(parent->a_children[a_index])) return a_index;
		
		// if not, determine the new number and fix the other index numbers too. Therefore use a reverse iterator,
		// because in normal case the index numbers from an arbitrary point to the end are invalid, and not
		// the index numbers from the beginning to a arbitrary point
		for (auto i = parent->a_children.rbegin(); i != parent->a_children.rend(); ++i)
		{
			// const auto current_index = i - parent->children.begin(); // if it would be a forward iterator
			const auto current_index = parent->a_children.rend() - i - 1;
			Derived* const ptr = cToPtr(*i);
			assert(ptr); // null must not occur here, because children have to unregister itself in the destructor
			ptr->a_index = static_cast<IdxT>(current_index);
			if (static_cast<const Derived*>(this) == ptr) return current_index;
		}
		
		assert(false); // if this happens, the list of children at the parent was incomplete (which is a bug somewhere, but not in this function), or maybe this object here is already destroyed
		
		return 0;
	}

	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	SharedT root() const
	{
		const Derived* current = static_cast<const Derived*>(this);
		while (pToPtr(current->a_parent)) current=pToPtr(current->a_parent);
		return const_cast<SharedT>(current);
	}

	template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	SharedT root() const
	{
		const Derived* current = static_cast<const Derived*>(this);
		while (pToPtr(current->a_parent)) current=pToPtr(current->a_parent);
		return const_cast<Derived*>(current)->shared_from_this();
	}
	
	
	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	SharedT up(unsigned int steps) const
	{
		const Derived* current = static_cast<const Derived*>(this);
		while (steps && pToPtr(current->a_parent)) { current=pToPtr(current->a_parent); --steps; }
		return const_cast<SharedT>(current);
	}

	template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	SharedT up(unsigned int steps) const
	{
		const Derived* current = static_cast<const Derived*>(this);
		while (steps && pToPtr(current->a_parent)) { current=pToPtr(current->a_parent); --steps; }
		return const_cast<Derived*>(current)->shared_from_this();
	}

	
	template <char x = anchor_mode, typename std::enable_if<x == 'R'>::type* = nullptr>
	inline SharedT parent() const { return a_parent.lock(); }
	template <char x = anchor_mode, typename std::enable_if<x != 'R'>::type* = nullptr>
	inline SharedT parent() const { return a_parent; }
	
	template <char x = anchor_mode, typename std::enable_if<x != 'L'>::type* = nullptr>
	inline SharedT child(IdxT index) const { return a_children[index]; }
	template <char x = anchor_mode, typename std::enable_if<x == 'L'>::type* = nullptr>
	inline SharedT child(IdxT index) const { return a_children[index].lock(); }

	template <char x = anchor_mode, typename std::enable_if<x != 'L'>::type* = nullptr>
	inline SharedT firstChild() const { return *a_children.crbegin(); }
	template <char x = anchor_mode, typename std::enable_if<x == 'L'>::type* = nullptr>
	inline SharedT firstChild() const { return *a_children.crbegin().lock(); }
	
	template <char x = anchor_mode, typename std::enable_if<x != 'L'>::type* = nullptr>
	inline SharedT lastChild() const { return *a_children.crbegin(); }
	template <char x = anchor_mode, typename std::enable_if<x == 'L'>::type* = nullptr>
	inline SharedT lastChild() const { return *a_children.crbegin().lock(); }
	
	inline const ChildrenT& children() const { return a_children; }
	inline size_t childCount() const { return a_children.size(); }

	template <char x = anchor_mode> // compile only if used
	void for_each(std::function<void (DerivedT& i, unsigned int relative_depth)> fnc)
	{
		std::vector<std::pair<typename ChildrenT::const_iterator, const typename ChildrenT::const_iterator>> itr_stack;
		itr_stack.emplace_back(std::make_pair(this->a_children.cbegin(), this->a_children.cend()));
		fnc(*static_cast<DerivedT*>(this), 0);
		
		while (true)
		{
			typename ChildrenT::const_iterator& itr = itr_stack.back().first;
			const typename ChildrenT::const_iterator& end = itr_stack.back().second;
		
			if (itr != end)
			{
				DerivedT* current = cToPtr(*itr++);
				fnc(*current, static_cast<unsigned int>(itr_stack.size()));
				itr_stack.emplace_back(std::make_pair(current->a_children.cbegin(), current->a_children.cend()));
			}
			else
			{
				itr_stack.pop_back();
				if (itr_stack.empty()) break;
			}
		}
	}
	
	template <char x = anchor_mode> // compile only if used
	unsigned int weight()
	{
		unsigned int result = 0;
		this->for_each([&result](DerivedT& i, unsigned int relative_depth) { ++result; });
		return result;
	}
	
	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	SharedT nextSibling() const
	{
		if (isRoot()) return nullptr;
		auto itr = a_parent->a_children.cbegin() + index() + 1;
		if (itr == a_parent->a_children.cend()) return nullptr;
		else return *itr;
	}
	
	template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	SharedT nextSibling() const
	{
		if (isRoot()) return SharedT();
		auto itr = pToPtr(a_parent)->a_children.cbegin() + index() + 1;
		if (itr == pToPtr(a_parent)->a_children.cend()) return SharedT();
		else return *itr;
	}
	
	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	SharedT previousSibling() const
	{
		if (isRoot()) return nullptr;
		auto itr = a_parent->a_children.cbegin() + index() - 1;
		if (itr == a_parent->a_children.cbegin()-1) return nullptr;
		else return *itr;
	}
	
	template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	SharedT previousSibling() const
	{
		if (isRoot()) return SharedT();
		auto itr = pToPtr(a_parent)->a_children.cbegin() + index() - 1;
		if (itr == pToPtr(a_parent)->a_children.cbegin()-1) return nullptr;
		else return *itr;
	}
	
	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	SharedT nextOnLevel() const
	{
		if (isRoot()) return nullptr;
		
		auto itr = a_parent->a_children.cbegin() + index() + 1;
		if (itr == a_parent->a_children.cend())
		{
			Derived* nextParent = a_parent->nextOnLevel();
			while(nextParent)
			{
				if (nextParent->childCount()) return nextParent->child(0);
				nextParent = nextParent->nextOnLevel();
			}
			return nullptr;
		}
		else return *itr;
	}
	
	template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	SharedT nextOnLevel() const
	{
		if (isRoot()) return SharedT();
		
		auto itr = pToPtr(a_parent)->a_children.cbegin() + index() + 1;
		if (itr == pToPtr(a_parent)->a_children.cend())
		{
			Derived* nextParent = pToPtr(pToPtr(a_parent)->nextOnLevel());
			while(nextParent)
			{
				if (nextParent->childCount()) return nextParent->child(0);
				nextParent = pToPtr(nextParent->nextOnLevel());
			}
			return SharedT();
		}
		else return *itr;
	}
	
	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	SharedT previousOnLevel() const
	{
		if (isRoot()) return nullptr;

		auto itr = a_parent->a_children.cbegin() + index() - 1;
		if (itr == a_parent->a_children.cbegin()-1)
		{
			Derived* previousParent = a_parent->previousOnLevel();
			while(previousParent)
			{
				if (previousParent->childCount()) return previousParent->child(previousParent->childCount()-1);
				previousParent = previousParent->previousOnLevel();
			}
			return nullptr;
		}
		else return *itr;
	}
	
	template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	SharedT previousOnLevel() const
	{
		if (isRoot()) return SharedT();
		
		auto itr = pToPtr(a_parent)->a_children.cbegin() + index() - 1;
		if (itr == pToPtr(a_parent)->a_children.cbegin()-1)
		{
			Derived* previousParent = pToPtr(pToPtr(a_parent)->previousOnLevel());
			while(previousParent)
			{
				if (previousParent->childCount()) return previousParent->child(previousParent->childCount()-1);
				previousParent = pToPtr(previousParent->previousOnLevel());
			}
			return SharedT();
		}
		else return *itr;
	}
	
	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	inline void untie()
	{
		moveChildrenTo(a_parent, index());
		moveTo(nullptr);
	}

	template <char x = anchor_mode, typename std::enable_if<x == 'R', char>::type* = nullptr>
	inline void untie()
	{
		moveChildrenTo(a_parent.lock(), index());
		moveTo(SharedT());
	}

	template <char x = anchor_mode, typename std::enable_if<x == 'L', char>::type* = nullptr>
	inline void untie()
	{
		moveChildrenTo(a_parent, index());
		moveTo(SharedT());
	}

	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	void prune()
	{
		for (auto i = a_children.begin(); i <a_children.end(); ++i)
		{
			(*i)->prune();
			delete *i;
		}
		a_children.clear();
	}

	template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	inline void prune()
	{
		moveChildrenTo(SharedT());
	}

	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	void moveTo(SharedT parent)
	{
		if (parent == static_cast<Derived*>(this)) return; // avoid infinite loops	
		if (parent == a_parent) return; // already established
		
		// remove from old parent
		if (this->a_parent) this->a_parent->a_children.erase(this->a_parent->a_children.begin() + index());
		this->a_parent = parent;
		
		// add to new parent
		if (parent) parent->a_children.push_back(static_cast<Derived*>(this));
	}
	
	template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	void moveTo(SharedT parent)
	{
		Derived* parent_to = parent.get();
		
		if (parent_to == static_cast<Derived*>(this)) return; // avoid infinite loop
		if (parent_to == pToPtr(a_parent)) return; // already established
		
		// ensure, that the counter of the shared pointer which owns this object does not decrement to 0
		// while doing the following things, which would delete this object
		SharedT me = this->shared_from_this();
		
		// remove from old parent
		Derived* const parent_from = pToPtr(this->a_parent);
		if (parent_from) parent_from->a_children.erase(parent_from->a_children.begin() + index());
		
		// add to new parent
		if (parent_to)
		{
			parent_to->a_children.push_back(me);
			this->a_parent = parent;
		}
		else this->a_parent = ParentT();
	}

	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	inline void separate() { moveTo(nullptr); ; }
	
	template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	inline void separate() { moveTo(SharedT()); }
	
	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	void moveTo(SharedT parent, IdxT index)
	{
		if (parent == static_cast<Derived*>(this)) return; // avoid infinite loop
		if (parent == a_parent) return; // already established
		
		// remove from old parent
		Derived* const parent_from = pToPtr(this->a_parent);
		if (parent_from) parent_from->a_children.erase(parent_from->a_children.begin() + this->index());
		
		// add to new parent
		if (parent)
		{
			if (static_cast<size_t>(index) >= parent->a_children.size()) parent->a_children.push_back(static_cast<Derived*>(this));
			else parent->a_children.insert(parent->a_children.begin()+static_cast<size_t>(index), static_cast<Derived*>(this));
			this->a_parent = parent;
		}
		else this->a_parent = ParentT();
	}
	
	template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	void moveTo(SharedT parent, IdxT index)
	{
		Derived* parent_to = parent.get();
		
		if (parent_to == static_cast<Derived*>(this)) return; // avoid infinite loop
		if (parent_to == pToPtr(a_parent)) return; // already established
		
		// ensure, that the counter of the shared pointer which owns this object does not decrement to 0
		// while doing the following things, which would delete this object
		SharedT me = this->shared_from_this();
		
		// remove from old parent
		Derived* const parent_from = pToPtr(this->a_parent);
		if (parent_from) parent_from->a_children.erase(parent_from->a_children.begin() + this->index());
		
		// add to new parent
		if (parent_to)
		{
			if (static_cast<size_t>(index) >= parent_to->a_children.size()) parent_to->a_children.push_back(me);
			else parent_to->a_children.insert(parent_to->a_children.begin()+static_cast<size_t>(index), me);
			this->a_parent = parent;
		}
		else this->a_parent = ParentT();
	}

	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	void moveChildrenTo(SharedT parent)
	{
		if (parent == static_cast<Derived*>(this)) return;
		if (parent)
		{
			for (auto i = a_children.begin(); i < a_children.end(); ++i) (*i)->a_parent = parent;
			
			parent->a_children.reserve(parent->a_children.size() + a_children.size());
			parent->a_children.insert(parent->a_children.end(), a_children.begin(), a_children.end());
		}
		else for (auto i = a_children.begin(); i < a_children.end(); ++i) (*i)->a_parent = nullptr;
		a_children.clear();
	}
	
	template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	void moveChildrenTo(SharedT parent)
	{
		Derived* parent_to = pToPtr(parent);
		if (parent_to == static_cast<Derived*>(this)) return;
		if (parent_to)
		{
			for (auto i = a_children.begin(); i < a_children.end(); ++i) cToPtr(*i)->a_parent = ParentT(parent);
			
			parent_to->a_children.reserve(parent_to->a_children.size() + a_children.size());
			parent_to->a_children.insert(parent_to->a_children.end(), a_children.begin(), a_children.end());
		}
		else for (auto i = a_children.begin(); i < a_children.end(); ++i) cToPtr(*i)->a_parent = ParentT();
		a_children.clear();
	}
	
	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	void moveChildrenTo(SharedT parent, IdxT index)
	{	
		if (parent == static_cast<Derived*>(this)) return;
		if (parent)
		{
			for (auto i = a_children.begin(); i < a_children.end(); ++i) (*i)->a_parent = parent;

			const IdxT psz = parent->a_children.size();
			parent->a_children.reserve(psz + a_children.size());
			
			if (index >= psz) parent->a_children.insert(parent->a_children.end(), a_children.begin(), a_children.end());
			else parent->a_children.insert(parent->a_children.begin() + static_cast<size_t>(index), a_children.begin(), a_children.end());
		}
		else for (auto i = a_children.begin(); i < a_children.end(); ++i) (*i)->a_parent = nullptr;
		a_children.clear();
	}
	
	template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	void moveChildrenTo(SharedT parent, IdxT index)
	{
		Derived* parent_to = pToPtr(parent);
		if (parent_to == static_cast<Derived*>(this)) return;
		if (parent_to)
		{
			for (auto i = a_children.begin(); i < a_children.end(); ++i) cToPtr(*i)->a_parent = ParentT(parent);

			const IdxT psz = parent_to->a_children.size();
			parent_to->a_children.reserve(psz + a_children.size());
			
			if (index >= psz) parent_to->a_children.insert(parent_to->a_children.end(), a_children.begin(), a_children.end());
			else parent_to->a_children.insert(parent_to->a_children.begin() + static_cast<size_t>(index), a_children.begin(), a_children.end());
		}
		else for (auto i = a_children.begin(); i < a_children.end(); ++i) cToPtr(*i)->a_parent = ParentT();
		a_children.clear();
	}
	
	template <char x = anchor_mode> // compile only if used
	IdxT reindex(IdxT index)
	{
		if (isRoot()) return 0;
		Derived* const parent = pToPtr(a_parent);
		const IdxT old_idx = this->index();
		const IdxT parentsz = static_cast<size_t>(parent->a_children.size());
		if (old_idx == index || (old_idx == parentsz-1 && index > old_idx) || index < 0) return old_idx;
		if (index >= parentsz) index = parentsz-1;

		const auto itr_old = parent->a_children.begin() + old_idx;
		const auto itr_new = parent->a_children.begin() + index;
		
		ChildT me = *itr_old;
		if (index < old_idx) std::move_backward(itr_new, itr_old, itr_old+1);
		else std::move(itr_old+1, itr_new+1, itr_old);
		*itr_new = me;
		return index;
		
		/*
		auto itr = parent->a_children.begin() + old_idx;
		
		parent->a_children.erase(itr);
		if (index >= parentsz)
		{
			parent->a_children.push_back(me);
			a_index = parentsz;
			return parentsz;
		}
		else
		{
			parent->a_children.insert(parent->a_children.begin()+index, me);
			a_index = index;
			return index;
		}
		
		*/
	}
	
	template <char x = anchor_mode> // compile only if used
	inline void swapChildren(IdxT index_a, IdxT index_b)
	{
		ChildT a = a_children[index_a];
		a_children[index_a] = a_children[index_b];
		a_children[index_b] = a;
	}

	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	void swapContent(Derived& other) noexcept
	{
		if (&other == this) return;
		this->a_children.swap(other.a_children);
		std::for_each<>(a_children.begin(), a_children.end(), [this](Derived* i){i->a_parent = static_cast<Derived*>(this);});
		std::for_each<>(other.a_children.begin(), other.a_children.end(), [&other](Derived* i){i->a_parent = &other;});
	}

	template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	void swapContent(Derived& other) noexcept
	{
		if (&other == this) return;
		// note: shared_from_this CAN throw an exception, but when it happens, then it is a bug in the code
		this->a_children.swap(other.a_children);
		std::for_each<>(a_children.begin(), a_children.end(), [this](ChildT& i){cToPtr(i)->a_parent = this->shared_from_this();});
		std::for_each<>(other.a_children.begin(), other.a_children.end(),
			[this,&other](ChildT& i){cToPtr(i)->a_parent = other.shared_from_this();}   );
	}

	template <bool deep = true, char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	void copyChildrenTo(Derived& parent) const
	{
		parent.a_children.reserve(parent.a_children.size() + a_children.size());
		for (typename ChildrenT::const_iterator i = a_children.cbegin(); i < a_children.cend(); ++i)
		{
			Derived* child = new Derived(**i);
			parent.a_children.push_back(child);
			child->a_parent = &parent;
			if (deep) (*i)->copyChildrenTo<true>(*child);
		}
	}

	template <bool deep = true, char x = anchor_mode, typename std::enable_if<x == 'R', char>::type* = nullptr>
	void copyChildrenTo(Derived& parent) const
	{
		parent.a_children.reserve(parent.a_children.size() + a_children.size());
		for (typename ChildrenT::const_iterator i = a_children.cbegin(); i < a_children.cend(); ++i)
		{
			SharedT child = std::make_shared<Derived>(*cToPtr(*i));
			parent.a_children.push_back(child);
			child->a_parent = parent.shared_from_this();
			if (deep) cToPtr(*i)->copyChildrenTo<true>(*child);
		}
	}
	
	// note: use like "copyChildrenTo(parent, std::back_inserter(vector));"
	template <bool deep = true, char x = anchor_mode, typename std::enable_if<x == 'L', char>::type* = nullptr, class OutputIterator>
	void copyChildrenTo(Derived& parent, OutputIterator leaf_insert_itr) const
	{
		parent.a_children.reserve(parent.a_children.size() + a_children.size());
		for (typename ChildrenT::const_iterator i = a_children.cbegin(); i < a_children.cend(); ++i)
		{
			SharedT child = std::make_shared<Derived>(*cToPtr(*i));
			parent.a_children.push_back(child);
			child->a_parent = parent.shared_from_this();
			if (deep && !cToPtr(*i)->isLeaf()) cToPtr(*i)->copyChildrenTo<true>(*child, leaf_insert_itr);
            else *leaf_insert_itr++ = child; // keep the leaf object, otherwise the shared pointer will expire
		}
	}
	
	template <bool deep = true, char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	IdxT copyChildrenTo(Derived& parent, IdxT index) const
	{
		IdxT previous_size = parent.a_children.size();
		if (index >= previous_size)
		{
			copyChildrenTo<deep>(parent);
			return previous_size;
		}
		
		IdxT result_index = index >= 0 ? index : 0;
		IdxT needed_space = a_children.size();
		parent.a_children.resize(previous_size + needed_space); // would invalidate all existing iterators; have to be done before the following itr arithmetics
		auto begin_itr = parent.a_children.begin()+result_index;
		const auto end_itr = parent.a_children.begin()+previous_size; 
		std::move_backward(begin_itr, end_itr, parent.a_children.end());
		
		for (typename ChildrenT::const_iterator i = a_children.cbegin(); i != a_children.cend(); ++i)
		{
			Derived* child = new Derived(**i);
			*begin_itr++ = child;
			child->a_parent = &parent;
			if (deep) (*i)->copyChildrenTo<true>(*child);
		}
		return result_index;
	}

	template <bool deep = true, char x = anchor_mode, typename std::enable_if<x == 'R', char>::type* = nullptr>
	IdxT copyChildrenTo(Derived& parent, IdxT index) const
	{
		IdxT previous_size = parent.a_children.size();
		if (index >= previous_size)
		{
			copyChildrenTo<deep>(parent);
			return previous_size;
		}
		
		IdxT result_index = index >= 0 ? index : 0;
		IdxT needed_space = a_children.size();
		parent.a_children.resize(previous_size + needed_space); // would invalidate all existing iterators; have to be done before the following itr arithmetics
		auto begin_itr = parent.a_children.begin()+result_index;
		const auto end_itr = parent.a_children.begin()+previous_size; 
		std::move_backward(begin_itr, end_itr, parent.a_children.end());
		
		for (typename ChildrenT::const_iterator i = a_children.cbegin(); i != a_children.cend(); ++i)
		{
			SharedT child = std::make_shared<Derived>(*cToPtr(*i));
			*begin_itr++ = child;
			child->a_parent = parent.shared_from_this();
			if (deep) cToPtr(*i)->copyChildrenTo<true>(*child);
		}
		return result_index;
	}
	
	// note: use like "copyChildrenTo(parent, index, std::back_inserter(vector));"
	template <bool deep = true, char x = anchor_mode, typename std::enable_if<x == 'L', char>::type* = nullptr, class OutputIterator>
	IdxT copyChildrenTo(Derived& parent, IdxT index, OutputIterator leaf_insert_itr) const
	{
		IdxT previous_size = parent.a_children.size();
		if (index >= previous_size)
		{
			copyChildrenTo<deep>(parent, leaf_insert_itr);
			return previous_size;
		}
		
		const IdxT result_index = index >= 0 ? index : 0;
		const IdxT needed_space = a_children.size();
		parent.a_children.resize(previous_size + needed_space); // would invalidate all existing iterators; have to be done before the following itr arithmetics
		auto begin_itr = parent.a_children.begin()+result_index;
		const auto end_itr = parent.a_children.begin()+previous_size; 
		std::move_backward(begin_itr, end_itr, parent.a_children.end());
		
		for (typename ChildrenT::const_iterator i = a_children.cbegin(); i != a_children.cend(); ++i)
		{
			SharedT child = std::make_shared<Derived>(*cToPtr(*i));
			*begin_itr++ = child;
			child->a_parent = parent.shared_from_this();
			if (deep && !cToPtr(*i)->isLeaf()) cToPtr(*i)->copyChildrenTo<true>(*child, leaf_insert_itr);
            else *leaf_insert_itr++ = child; // keep the leaf object, otherwise the shared pointer will expire
		}
		return result_index;
	}
	
	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	inline SharedT deepCopy() const
	{
		Derived* result = new_(*this);
		this->copyChildrenTo<true>(*result);
		return result;
	}

	template <char x = anchor_mode, typename std::enable_if<x == 'R', char>::type* = nullptr>
	SharedT deepCopy() const
	{
		SharedT result = new_(*this);
		this->copyChildrenTo<true>(*result);
		return result;
	}
	
	// note: use like "deepCopy(std::back_inserter(vector));"
	template <char x = anchor_mode, typename std::enable_if<x == 'L', char>::type* = nullptr, class OutputIterator>
	SharedT deepCopy(OutputIterator leaf_insert_itr) const
	{
		SharedT result = new_(*this);
		this->copyChildrenTo<true>(*result, leaf_insert_itr);
		return result;
	}

	template <char x = anchor_mode, typename std::enable_if<x == 'P', char>::type* = nullptr>
	SeqTreeClass(SeqTreeClass&& other) noexcept :
		a_parent(nullptr), a_children(std::move(other.a_children)), a_index(0)
	{
		std::for_each<>(a_children.begin(), a_children.end(), [this](ChildT& i){i->a_parent = this;});
	}

	// shared_from_this does not work during construction (but would be needed for the parent attribute at children)
	// workaround: make an empty object, then use operator=(&&)
	template <char x = anchor_mode, typename std::enable_if<x != 'P', char>::type* = nullptr>
	SeqTreeClass(SeqTreeClass&& other) noexcept = delete;
	
	SeqTreeClass<Derived, IdxT, anchor_mode>& operator=(SeqTreeClass && rhs) noexcept
	{
		if (this != &rhs)
		{
			// remove the old content
			prune();
			
			// keep the tree where rhs was attached clean
			rhs.separate();
			
			// move the children
			this->a_children = std::move(rhs.a_children);
			
			// tell the new parent
			std::for_each<>(a_children.begin(), a_children.end(), [this](ChildT& i){cToPtr(i)->a_parent = this_();});
		}
		return *this;
	}

	SeqTreeClass(const SeqTreeClass & other) = delete; // no deep copies here, and the children are the only meaningful attributes ; use default constructor
	SeqTreeClass& operator=(const SeqTreeClass & rhs) = delete; // deep copies are not wanted, and the children are the only meaningful attributes
	
	template <char x = anchor_mode> // compile only if used
	unsigned int depth() const
	{
		const Derived* current = static_cast<const Derived*>(this);
		unsigned int result = 0;
		while (pToPtr(current->a_parent))
		{
			++result;
			current=pToPtr(current->a_parent);
		}
		return result;
	}

	template <char x = anchor_mode, typename std::enable_if<x == 'R'>::type* = nullptr>
	inline bool isRoot() const { return a_parent.expired(); }
	template <char x = anchor_mode, typename std::enable_if<x != 'R'>::type* = nullptr>
	inline bool isRoot() const { return !a_parent; }
	
	template <char x = anchor_mode> // compile only if used
	inline bool isLeaf() const { return a_children.empty(); }
	
protected:
	ParentT a_parent;
	ChildrenT a_children;
	
private:
	mutable IdxT a_index;
	
	// (c)hild to pointer  and  (p)arent to pointer
	
	template <char x = anchor_mode, typename std::enable_if<x == 'P'>::type* = nullptr>
	inline Derived*& pToPtr(SharedT& ptr) const { return ptr; }
	template <char x = anchor_mode, typename std::enable_if<x == 'P'>::type* = nullptr>
	inline Derived*& cToPtr(SharedT& ptr) const { return ptr; }
	template <char x = anchor_mode, typename std::enable_if<x == 'P'>::type* = nullptr>
	inline Derived* const & pToPtr(SharedT const & ptr) const { return ptr; }
	template <char x = anchor_mode, typename std::enable_if<x == 'P'>::type* = nullptr>
	inline Derived* const & cToPtr(SharedT const & ptr) const { return ptr; }
	
	template <char x = anchor_mode, typename std::enable_if<x == 'R'>::type* = nullptr>
	inline Derived* pToPtr(const ParentT ptr) const { return ptr.lock().get(); }
	template <char x = anchor_mode, typename std::enable_if<x == 'R'>::type* = nullptr>
	inline Derived* cToPtr(const ChildT ptr) const { return ptr.get(); }

	template <char x = anchor_mode, typename std::enable_if<x == 'L'>::type* = nullptr>
	inline Derived* pToPtr(const ParentT ptr) const { return ptr.get(); }
	template <char x = anchor_mode, typename std::enable_if<x == 'L'>::type* = nullptr>
	inline Derived* cToPtr(const ChildT ptr) const { return ptr.lock().get(); }
	
	template <char x = anchor_mode, typename std::enable_if<x == 'P'>::type* = nullptr >
	inline SharedT new_(const SeqTreeClass& other) const { return new Derived(static_cast<const Derived&>(other)); }
	template <char x = anchor_mode, typename std::enable_if<x != 'P'>::type* = nullptr >
	inline SharedT new_(const SeqTreeClass& other) const { return std::make_shared<Derived>(static_cast<const Derived&>(other)); }
	
	template <char x = anchor_mode, typename std::enable_if<x == 'P'>::type* = nullptr >
	inline Derived* this_() { return static_cast<Derived*>(this); }
// 	template <char x = anchor_mode, typename std::enable_if<x == 'P'>::type* = nullptr >
// 	inline const Derived* this_() const { return static_cast<const Derived*>(this); }
	template <char x = anchor_mode, typename std::enable_if<x != 'P'>::type* = nullptr >
	inline SharedT this_() { return this->shared_from_this(); }
// 	template <char x = anchor_mode, typename std::enable_if<x != 'P'>::type* = nullptr >
// 	inline const SharedT this_() const { return this->shared_from_this(); }
};
